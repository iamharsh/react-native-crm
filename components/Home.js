import React, { Component } from 'react'
import { Text, StyleSheet, View, Button } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'

export default class Home extends Component {
    render() {
        return (
            <View>
                <Text> HomeHarsh </Text>
                <Icon name='home' size={30} color="#bbb"/>
                <Button
                    title="Go to Setting"
                    onPress={() => this.props.navigation.navigate('Setting')}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({})
