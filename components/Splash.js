import React, { Component } from 'react'
import { Text, StyleSheet, View, ActivityIndicator, Button, Image } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

export default class Splash extends Component {
    componentDidMount() {
        setTimeout(() => {
            this.props.navigation.navigate('Home')
        }, 3000);

    }
    render() {
        return (
            <View style={{ flex: 1 }}>
                <LinearGradient style={{ flex: 1 }} start={{ x: 0, y: 1 }} end={{ x: 1, y: 0 }} colors={['#5F57C9', '#5D89E4']}>
                    <View style={styles.container}>
                        <Image onPress={() => this.navigate(bind)} style={{ width: 250, height: 30 }} source={require('../assets/logo.png')} />
                        <ActivityIndicator style={{ marginTop: 20 }} size="large" color="#fff" />
                        <Button
                            title="Tap"
                            onPress={() => this.props.navigation.navigate('Home')}
                        />
                    </View>
                </LinearGradient>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    }
})
