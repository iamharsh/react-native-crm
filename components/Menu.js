import React, { Component } from 'react'
import { Text, StyleSheet, View, SafeAreaView, ScrollView } from 'react-native'
import Icon from 'react-native-vector-icons/Feather';
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'

export default class Menu extends Component {
    render() {
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView>
                    <View style={{ flexDirection: 'row', marginTop: 20, alignItems: 'center',marginVertical: 10 }}>
                        <View style={{ marginLeft: 20, width: 30 }}>
                            <Icon name='crosshair' size={20} color='blue' />
                        </View>
                        <View>
                            <Text style={{ fontSize: 16, fontWeight: '600', marginLeft: 15 }}>Leads</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 20, alignItems: 'center',marginVertical: 10 }}>
                        <View style={{ marginLeft: 20, width: 30 }}>
                            <Icon name='activity' size={20} color='blue' />
                        </View>
                        <View>
                            <Text style={{ fontSize: 16, fontWeight: '600', marginLeft: 15 }}>Activities</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 20, alignItems: 'center',marginVertical: 10 }}>
                        <View style={{ marginLeft: 20, width: 30 }}>
                            <Icon name='briefcase' size={20} color='blue' />
                        </View>
                        <View>
                            <Text style={{ fontSize: 16, fontWeight: '600', marginLeft: 15 }}>Projects</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 20, alignItems: 'center',marginVertical: 10 }}>
                        <View style={{ marginLeft: 20, width: 30 }}>
                            <Icon name='edit' size={20} color='blue' />
                        </View>
                        <View>
                            <Text style={{ fontSize: 16, fontWeight: '600', marginLeft: 15 }}>Tasks</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 20, alignItems: 'center',marginVertical: 10 }}>
                        <View style={{ marginLeft: 20, width: 30 }}>
                            <Icon name='trending-up' size={20} color='blue' />
                        </View>
                        <View>
                            <Text style={{ fontSize: 16, fontWeight: '600', marginLeft: 15 }}>Reports</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 20, alignItems: 'center',marginVertical: 10 }}>
                        <View style={{ marginLeft: 20, width: 30 }}>
                            <Icon name='users' size={20} color='blue' />
                        </View>
                        <View>
                            <Text style={{ fontSize: 16, fontWeight: '600', marginLeft: 15 }}>Team & Roles</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 20, alignItems: 'center',marginVertical: 10 }}>
                        <View style={{ marginLeft: 20, width: 30 }}>
                            <FontAwesome name='dollar' size={20} color='blue' />
                        </View>
                        <View>
                            <Text style={{ fontSize: 16, fontWeight: '600', marginLeft: 15 }}>Deals</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 20, alignItems: 'center',marginVertical: 10 }}>
                        <View style={{ marginLeft: 20, width: 30 }}>
                            <FontAwesome name='building-o' size={20} color='blue' />
                        </View>
                        <View>
                            <Text style={{ fontSize: 16, fontWeight: '600', marginLeft: 15 }}>Companies</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 20, alignItems: 'center',marginVertical: 10 }}>
                        <View style={{ marginLeft: 20, width: 30 }}>
                            <Icon name='settings' size={20} color='blue' />
                        </View>
                        <View>
                            <Text style={{ fontSize: 16, fontWeight: '600', marginLeft: 15 }}>Settings</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 20, alignItems: 'center',marginVertical: 10}}>
                        <View style={{ marginLeft: 20, width: 30 }}>
                            <MaterialIcons name='group' size={20} color='blue' />
                        </View>
                        <View>
                            <Text style={{ fontSize: 16, fontWeight: '600', marginLeft: 15 }}>People</Text>
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    }
})
