import React, { Component } from 'react'
import { Text, StyleSheet, View, Button } from 'react-native'

export default class Setting extends Component {
    render() {
        return (
            <View>
                <Text> Settings </Text>
                <Button
                    title="Go to Setting"
                    onPress={() => this.props.navigation.navigate('Lead')}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({})
