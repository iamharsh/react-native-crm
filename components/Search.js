import React, { Component } from 'react'
import { Text, StyleSheet, View, SafeAreaView, TextInput } from 'react-native'
import Icon from 'react-native-vector-icons/Feather'

export default class Search extends Component {
    render() {
        return (
            <SafeAreaView style={styles.container}>                
                <View style={styles.search}>
                    <TextInput
                        style={{ height: 50, width: 350, borderColor: 'gray', borderWidth: 1, borderRadius: 5 }}
                        onChangeText={text => onChangeText(text)}
                        value='Harsh'
                    />
                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    search: {
        backgroundColor: '#bbb',
        height: 50
    }
})
