import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import Icon from 'react-native-vector-icons/MaterialIcons'
import EvilIcons from 'react-native-vector-icons/EvilIcons'

import LinearGradient from 'react-native-linear-gradient';

import Home from './components/Home';
import Setting from './components/Setting';
import Lead from './components/Lead';
import Splash from './components/Splash';
import Search from './components/Search';
import Notification from './components/Notification';
import Menu from './components/Menu';


const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Splash" component={Splash}
          options={{
            headerShown: false
          }} />
        <Stack.Screen name="Home" component={TabBar}
          options={{
            headerLeft: () => (
              <EvilIcons
                name="gear"
                size={30}
                color="#6D73D3"
                style={{ marginLeft: 10 }}
              />
            ),
            headerRight: () => (
              <View style={{flexDirection: 'row', marginRight: 10}}>
                <View style={{ height: 9, width: 9, backgroundColor: '#00B5B4', marginRight: 4, borderRadius: 50 }}></View>
                <View style={{ height: 9, width: 9, backgroundColor: '#FF463B', marginRight: 4, borderRadius: 50 }}></View>
                <View style={{ height: 9, width: 9, backgroundColor: '#FF7834', marginRight: 4, borderRadius: 50 }}></View>
              </View>
            ),
            headerTitleAlign: 'center',
            title: 'Dashboard',
          }} />
        <Stack.Screen name="Setting" component={Setting} />
        <Stack.Screen name="Lead" component={Lead} />
        <Stack.Screen name="Search" component={Search} />
        <Stack.Screen name="Notification" component={Notification} />
        <Stack.Screen name="Menu" component={Menu} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const Tab = createBottomTabNavigator();

function TabBar() {
  return (
    <Tab.Navigator tabBarOptions={{
      showLabel: false,
      style: { height: 60 }
    }}>
      <Tab.Screen name="Home" component={Home}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({ color, size }) => (
            <Icon name="home" color={color} size={size} />
          ),
        }} />
      <Tab.Screen name="Notification" component={Notification}
        options={{
          tabBarLabel: 'Notification',
          tabBarIcon: ({ color, size }) => (
            <Icon name="notifications-none" color={color} size={size} />
          ),
        }} />
      <Tab.Screen name="Lead" component={Lead}
        options={{
          tabBarLabel: 'Lead',
          tabBarIcon: ({ color, size }) => (
            <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
              colors={['#5F57C9', '#5D89E4']}
              style={{ padding: 12, borderRadius: 50 }}>
              <Icon name="add" color='#fff' size={size} />
            </LinearGradient>
          ),
        }} />
      <Tab.Screen name="Search" component={Search}
        options={{
          tabBarLabel: 'Search',
          tabBarIcon: ({ color, size }) => (
            <Icon name="search" color={color} size={size} />
          ),
        }} />
      <Tab.Screen name="Menu" component={Menu}
        options={{
          tabBarLabel: 'Menu',
          tabBarIcon: ({ color, size }) => (
            <Icon name="menu" color={color} size={size} />
          ),
        }} />
    </Tab.Navigator>
  )
}

const Drawer = createDrawerNavigator();

function DrawerNav() {
  return (
    <Drawer.Navigator initialRouteName="Home">
      <Drawer.Screen name="Home" component={Home} />
      <Drawer.Screen name="Setting" component={Setting} />
    </Drawer.Navigator>
  )
}

export default App;

const styles = StyleSheet.create({})
